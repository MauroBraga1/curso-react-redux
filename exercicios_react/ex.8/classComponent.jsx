import React, {Component} from 'react'


export default class ClassComponent extends Component{
    //função obrigatória
    render(){
        return(
            <h1>{this.props.value}</h1>
        )
    }
}