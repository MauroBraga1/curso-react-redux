import 'bootstrap/dist/css/bootstrap.min.css'
import 'font-awesome/css/font-awesome.min.css'
import './App.css'
import React  from 'react'
import {BrowserRouter} from 'react-router-dom'

import Routes from './routes'
import Logo from '../components/template/logo.jsx'
import Nav  from '../components/template/nav.jsx'
import Footer from  '../components/template/footer'

export default props =>
    <BrowserRouter>
        <div className="app">
            <Logo/>
            <Nav/>
            <Routes/>
            <Footer/>
        </div>
    </BrowserRouter>