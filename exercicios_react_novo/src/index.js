import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';
import PrimeiroComponente from './components/PrimeiroComponente';
import {CompA as A, CompB as B} from './components/DoisComponentes';
import MultiElements        from './components/MultiElements';
import FamiliaSilva         from './components/FamiliaSilva';
import Familia              from './components/Familia';
import Membro               from './components/Membro';
import ComponenteComFuncao  from './components/ComponenteComFuncao';
import Pai                  from './components/Pai'
import ComponenteClasse     from './components/ComponenteClasse';
import Contador             from './components/Contador';
import Hook                 from './components/Hook';

ReactDOM.render(
    <div>
        <Hook/>
        <Contador numeroInicial={0}/>
        <ComponenteClasse valor="Sou um componente de classe !"/>
        <Pai/>    
        <ComponenteComFuncao/>

        <Familia sobrenome= "Silveira">
            <Membro nome="Andre" />
            <Membro nome="Silveira" />
        </Familia>


        <MultiElements/>
        <PrimeiroComponente valor="Bom dia" aBcD={2**8}/>
        <A valor="Olá eu sou A"/>
        <B valor="Olá eu sou B"/>
        <FamiliaSilva sobrenome="Silva"/>
    </div>    
    , document.getElementById('root'));

serviceWorker.unregister();
