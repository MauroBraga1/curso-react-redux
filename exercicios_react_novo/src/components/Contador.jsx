import React, {Component} from 'react';

export default class Contador extends Component{

    state ={
        numero : this.props.numeroInicial
    }

    /*
    constructor(props){
        super(props);
        this.maisUm=this.maisUm.bind(this);
    }*/

    maisUm=()=>{
        this.setState({numero:this.state.numero+1})
        
    }

    menosUm = () =>{
        this.setState({numero:this.state.numero+1})
        
    }

    render(){
        return(
            <div>
                <div>Número: {this.state.numero}</div>
                <button onClick={this.maisUm}>Inc</button>
                <button onClick={this.menosUm}>Dec</button>
            </div>
        )
    }
}