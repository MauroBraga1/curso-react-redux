import React from 'react'
import ReactDom from 'react-dom'
import Pai from './componentes/pai'
import Filho from './componentes/filho'
ReactDom.render(
            <div>
                <Pai sobrenome="Braga" nome ="Mauro">
                    <Filho nome='Pedro' />
                    
                    <Filho nome='Amanda'/>
                    <Filho nome='Clara' />
                </Pai>    
            </div>
            , document.getElementById('root'))